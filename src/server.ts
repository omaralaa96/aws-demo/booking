import "reflect-metadata";
import { createExpressServer } from 'routing-controllers';
import { bookingController } from './BookingComponent/BookingController';
import * as bodyParser from 'body-parser';

class Server{
    public static serverInstance:Server;
    private server : any;

    public static bootstrap(): Server{
        if(!this.serverInstance){
            this.serverInstance = new Server();
        }
        return this.serverInstance;
    }

    private async createServer(){
        const app = createExpressServer({
            controllers:[
                bookingController
            ]
        });

        app.use(bodyParser.json());
        const port = process.env.PORT || 3000;
        this.server = await app.listen(port, () => {
            console.log(`App listening on the port ` + port);
        });
    }

    public constructor(){
        this.createServer();
    }
}

export const server = Server.bootstrap();