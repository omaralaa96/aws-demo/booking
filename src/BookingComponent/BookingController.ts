import { Request, Response } from 'express'; 
import { JsonController, Get, Req, Res } from 'routing-controllers';
import { BookingService } from './BookingService';
require('dotenv').config();

@JsonController("/bookings")
export class bookingController {

    readonly service = new BookingService();

    @Get("")
    async getUsersAndHotels(@Req() request: Request, @Res() response: Response){
        const res = await this.service.getUsersAndHotels();
        response.send(res);
        return response;
    } 
}