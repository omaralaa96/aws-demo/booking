const axios = require('axios');

export class BookingService{

    async getUsersAndHotels(): Promise<any> {
        try{
            let hotelResponse = await axios.get(`${process.env.HOTELS_SERVICE}/hotels`);
            // console.log(hotelResponse.data);
            let userResponse = await axios.get(`${process.env.USERS_SERVICE}/users`);
            // console.log(userResponse.data);
            let response = {};
            response["hotels"] = hotelResponse.data;
            response["users"] = userResponse.data;
            return response;
        }
        catch(error){
            console.log(error);
        }        
    }
}